TestIterator:
Linked list is faster (268 ms) than array list (309 ms) 

Using list.remove(77); , the index at 77 is out of bound, so it gives an out of bound error. Size is still 7, so I know that the number 77 wasn't remove.

TestList:
Linked list (196 ms) was faster than Array list(202 ms). 

TestPerformance: 

SIZE 10 = 218 ms;
100 = 252 ms;
1000 = 1s 389 ms;
10000 = 12s 294 ms;  